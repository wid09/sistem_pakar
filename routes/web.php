<?php

use App\Http\Controllers\Dashboard\Admin_PenyakitController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KonsultasiController;
use App\Http\Controllers\Dashboard\GejalaController;
use App\Http\Controllers\Dashboard\PenyakitController;

Route::get('/tes', function () {
    return view('components.dashboard');
});
Route::get('/', [HomeController::class,'index']);
Route::get('penyakit', [HomeController::class, 'penyakit'])->name('penyakit');
Route::get('penyakit/detail/{penyakit}', [HomeController::class,'detail'])->name('detail');
Route::get('gejala', [HomeController::class,'gejala'])->name('gejala');
Route::get('tentang', function(){
    return view('tentang');
})->name('tentang');

Route::group(['prefix' => 'konsultasi'], function () {
    Route::get('/' , [KonsultasiController::class,'pasienForm'])->name('pasienForm');
    Route::post('/', [KonsultasiController::class,'storeForm'])->name('storeForm');
    Route::post('diagnosa', [KonsultasiController::class,'diagnosa'])->name('diagnosa');
    Route::get('{pasien_id}/hasil', [KonsultasiController::class,'hasilDiagnosa'])->name('hasilDiagnosa');
});

Route::group(['prefix' => 'dashboard'], function() {
    Route::get('/', [Admin_PenyakitController::class,'dashboard']);
    Route::get('/penyakit', [Admin_PenyakitController::class,'index'])->name('admin.penyakit');
    Route::get('penyakit/{penyakit}/edit', [Admin_PenyakitController::class,'edit'])->name('penyakit.edit');
    Route::post('penyakit_store', [Admin_PenyakitController::class,'store'])->name('penyakit.store');
    Route::patch('penyakit_update/{penyakit}', [Admin_PenyakitController::class,'update'])->name('penyakit.update');
    Route::delete('penyakit_delete/{penyakit}', [Admin_PenyakitController::class,'destroy'])->name('penyakit.destroy');
    Route::get('/gejala', [GejalaController::class,'index'])->name('admin.gejala');
    Route::post('gejala', [GejalaController::class,'store'])->name('store');
    Route::patch('gejala_update/{gejala}', [GejalaController::class,'update'])->name('gejala.update');
    Route::delete('gejala_delete/{gejala}', [GejalaController::class,'destroy'])->name('gejala.destroy');
    Route::get('tentang', function(){
        return view('tentang');
    })->name('admin.tentang');
});

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     Route::get('/', [Admin_PenyakitController::class]);
// })->name('dashboard');
